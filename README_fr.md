


A faire :
Photos shield & digital board, fléchées avec les différents blocs
Placer des capteurs sur les schémas d'instal élec, + forme du courant à chaque étape + points chauds du circuit

Dessin gilou où placer les capteurs
Dessin Hardware / Firmware / Software

PHOTOS
branchements anémo ils/Hall
Tout le matos

# Présentation du projet et installation du datalogger
## Le projet de windlogger

Le windlogger est un projet open-source d'enregistreur de données pour le petit éolien. Cette idée à émergée lors de discution avec les membres du réseau Tripalium courant 2014. Les outils éxistants à l'époque , étaient soit chers, soit peu adaptés aux mesures que nous souhaitions faire. La présentation d'un premier prototype lors des conférences WEAthens2014 organisés par l'association WindEmpowerment à confirmé la nécessité de cette initiative.

Le principe est de développer un outil simple et abordable pour effectuer des mesures de vent et de puissance d'une éolienne. Ces données peuvent être stockées sur carte SD ou bien transmises (USB, Wifi, GPRS).

Actuellement les données courantes sur le vent sont inadaptées au petit éolien puisque la plupart des données sont données pour des hauteur de 50 ou 80 m. De plus, le coût prohibitifs d'une campagne de mesure nous empèche d'y procéder.

Une autre motivation est que les pilotes d'éolienne souhaitent avoir un outil de visualisation de leur site en termes de ressources vent, et de production. Puisque nous construisons nos éoliennes, cela permet aussi lors d'une mise en service d'une nouvelle éolienne, de vérifier son bon fonctionnement, est-ce que l'éolienne produit trop ou assez? Est-ce que la régulation fonctionne correctement?

Nous sommes donc partis sur une solution libre et modulable pour répondre aux différentes configurations possible.


## Les acteurs
### ALEEA
![logo association Aleea](images/logo_aleea.png "logo Aleea")*logo association Aleea*

Basée dans la région toulousaine, l’association ALEEA a pour objet de rechercher, développer, fabriquer et promouvoir l'électronique pour la gestion de l'énergie auto-produite.

Elle défend la liberté d'utiliser, étudier, copier, modifier et fabriquer les systèmes électroniques qu'elle a conçus dans un esprit de « Hardware libre » tel qu'il est présenté sur le site gnu.org.

Elle développe des actions d'échanges de savoirs et de savoir-faire s'inscrivant dans une démarche de développement de l'autonomie des personnes vis-à-vis des systèmes électroniques et de production d'énergie.

#### Tripalium
![logo association Tripalium](images/logo_tripalium.png "logo Tripalium")*logo association tripalium*

Impulsée en 2007 par la volonté de développer les stages d’auto-construction, l’association Tripalium s’est transformée en un réseau d’acteur-rices. Sans salarié ni subvention, sa vitalité repose sur la dynamique des stages : les anciens stagiaires sont invités à devenir formateurs, puis à organiser des stages...

En 2014, l’association, compte à son actif l’organisation d’une centaine de stages. Une douzaine de formateurs et formatrices se répartit principalement en Drôme, Haute-Garonne, Lozère, Gard, Loire-Atlantique, Tarn, Finistère, Ardèche. Et plus de 150 éoliennes Piggott ont déjà été construites en France. Les principales activités du réseau sont : les stages, les rencontres créatives (annuelles), l’aide à l’organisation de stages et à l’investissement, le site web, la traduction, l’actualisation et la publication du manuel, des interventions dans divers événements et sous diverses formes.

#### WindEmpowerment
![logo association WindEmpowerment](images/logo_WindEmpowerment.png "logo WindEmpowerment")*logo association WindEmpowerment*

Windempowerment est une association pour le développement de la fabrication locale d'éolienne de type Piggott pour l'électrification rurale. Nous représentons des dizaines d'organisations membres, constituées de fabricants, d'ONG, d'universités, entreprises sociales, de centre de formations  ainsi que plus de 1000 participants individuels à travers le monde.

#### Contributeurs et contributrices

 * LONGUET Gilles, development/maintener(@gilou_)
 * Luiz Villa, development(@luiz.villa)
 * DAME David, contributor
 * HEMBERT Brice, contributor
 * BENHAMED Sofiane, contributor
 * GILBERT Aurélie, Documentation(@aurelieguibert)

## Combien ça coûte ?
Actuellement l'association Aleea vend les premiers kits, cela comprend les cartes **Digital** et **Shield**, des catpeurs pour un wattmètre (DC ou AC) et le boîtier. Comme nous travaillons au cas par cas actuellement, veuillez nous contacter.
contact : contact (at) aleea (dot) org

## Comment ça fonctionne ?
### Rappels sur l'installation électrique d'une éolienne

!["Schéma d'u site isolé par Eve Lomenech"](images/Sch_site_isole_auteure_eve_lomenech.png "Schéma d'un site isolé par Eve Lomenech")*Schéma d'un site isolé par Eve Lomenech*


#### La génératrice
Génére un courant alternatif triphasé à fréquence variable, selon la puissance du vent.
Ce courant à fréquence variable n'est pas utilisable tel quel, il nécessite un passage par différents boitiers électroniques.

#### Le frein
Il permet de freiner l'éolienne en mettant les 3 phases de la génératrice en court-circuit. On l'utilise pour le levage et la descente de l'éolienne.

#### Le pont de diodes, ou pont redresseur
Il permet de transformer un courant alternatif en courant continu.
C'est après ce pont de diodes qu'on branchera les batteries pour stocker l'énergie dans le cas d'un site isolé.
(Le pont de diodes est fixé sur un «  radiateur  » en aluminium pouvant dissiper la chaleur produite.)

#### Le régulateur
Dans le cas d'un site isolé, il protège les batteries. Quand elles sont pleines, l'énergie doit obligatoirement s'échapper quelque part (il ne faut jamais que l’éolienne tourne à vide). Le régulateur commute donc cette énergie dans la résistance de décharge.

Dans le cas d'un site raccordé au réseau, il protège l’onduleur des surtensions par délestage dans une résistance.
Il protège aussi l’éolienne en cas de coupure du réseau électrique, en connectant l’éolienne sur la résistance pour éviter l’emballement.

#### L'onduleur
L'onduleur transforme le courant continu en courant alternatif 230V / 50Hz, que l'on pourra directement utiliser ou injecter sur le réseau électrique dans le cas d'un raccordement réseau.

Pour plus de détails, consulter le [manuel Tripalium](https://www.tripalium.org/manuel).

### L’installation du boîtier et des capteurs

![Schéma d'un site isolé par Eve Lomenech, colorisé par Aurélie Guibert](images/Sch_site_isole_capteurs_auteure_eve_lomenech.png "Schéma d'un site isolé par Eve Lomenech, colorisé par Aurélie Guibert")*Schéma d'un site isolé par Eve Lomenech, colorisé par Aurélie Guibert*

## Les cartes électroniques (Hardware)

### La partie numérique digital board :
La carte *digital* reçoit organise les mesures et gère l'enregistrement des données ainsi que leurs transmition.
De base, cette carte peut enregistrer les données sur carte SD et les envoyer par USB.

**Les modules Wifi et GPRS sont en option.**

!["Carte digital"](images/board_digital.png "Carte digital")*Carte digital*


### La partie analogique shield board :
Elle vient s’imbriquer sur la carte *digital*. C'est elle qui reçoit les différents capteurs, elle met en forme leurs signaux pour pouvoir les lire avec le micro-contrôleur de la carte *digital*.

!["Carte shield"](images/board_shield.png "Carte shield")*Carte shield*

## Le code du micro-contrôleur (« Firmware »)

Le firmware est développer pour être compatible avec l'environnement Arduino. Cela le rend donc accessible et évolutif.

## Le traitement des données (« Software ») - en cours de développement

La partie "Software" permet d'analyser et de visualiser les données sur un ordinateur. (rose des vents dominants (puissance/fréquence), graphiques de production sur l'année, production de l'éolienne en fonction des vitesses de vent, détection de disfonctionnements et corrélation avec différents facteurs, etc.)

**Pour l'instant nous ne nous sommes pas occupé de cette partie.**

**Si cette partie vous intéresse, n'hésitez pas à nous contacter.**

### Les outils / matériel
++Photo du matos fléchée

### L'alimentation

### Les anémomètres et la girouette
Il est possible d'installer jusqu'à deux anémomètres et une girouette.

#### Quels anémomètres choisir ?
Nous vous conseillons les modèles suivants :

 - NRGsystems :
 - - [NRG40C](https://www.nrgsystems.com/products/met-sensors/anemometers/detail/40c-anemometer)
 - - [NRG40H](https://www.nrgsystems.com/products/met-sensors/anemometers/detail/40h-anemometer-hall-effect)
 - Inspeed Vortex :
 - - [Inspeed Vortex II](http://inspeed.com/anemometers/Vortex_II_Wind_Sensor.asp)
 - - [Inspeed Vortex](http://inspeed.com/anemometers/Vortex_Wind_Sensor.asp)

#### Quelle girouette choisir ?
Une girouette fonctionne à l'aide d'un potentiomètre (résistance variable à trois bornes). Les deux bornes extérieures déterminent la résistance maximale.
Celle du milieu est un curseur qui fait varier la valeur de la résistance. C'est par elle qu'on recueillera la valeur de l'angle donné par la girouette.

Nous vous conseillons les modèles suivants :

 - [NRG200P](https://www.nrgsystems.com/products/met-sensors/wind-direction-vanes/detail/200p-wind-vane)
 - [Inspeed E-Vane](http://inspeed.com/wind_speed_direction/Vane.asp)

#### Où les placer ?
Si vous choisissez d'installer l'anémomètre et la girouette sur un mât indépendant de l'éolienne, veillez à reconstituer les mêmes conditions (proche de l'éolienne, à la même hauteur) pour pouvoir exploiter facilement les mesures. Pour un mât de mesure en prévision d'un future installation, placez-le à l'endroit exact où vous comptez installer l'éolienne.

Si votre éolienne est déjà installée, il est possible de les positionner directement sur le mât, sur une barre horizontale à 1m en dessous de l'hélice. On évitera ainsi de perturber les mesures en captant les turbulences provoquées par l'hélice.

(++dessin gilou, faire un dessin plus détaillé qui montre les obstacles)
![alt text](images/environnement_s.svg)

#### Comment les brancher ?
Les anémomètres (bornes Anemo1 & Anemo2) sont connectés aux broches 1 et 2, les pôles + et - pouvant être inversés.
NB : Selon le type de capteur choisi, on branchera le cavalier :

 - En haut pour un capteur à effet Hall
 - En bas pour un capteur ILS

Effet hall : 1 et 2
ILS : 1,2,3

++Photo des 2 bornes avec les stickers
++Photo des connexions selon effet Hall/ils

La girouette (borne Vane) est connectée avec le pôle - sur la broche 1 et le pôle + sur la broche 3. La borne du milieu, qui communique la valeur de l'angle indiqué par la girouette, se branche sur la broche 2.

++Photo de la borne avec les stickers

#### Comment les calibrer ?

++Lien calibrage anémo
++Lien calibrage girouette

### Capteurs de puissance
Il est possible d'installer jusqu'à deux sondes de tension (une AC et une DC) et quatre sondes de courant (AC et/ou DC).

#### Quelles sondes choisir ?
##### Tension continue
Deux fils branchés sur votre parc de batteries suffisent.

++Photo branchement ?

##### Tension alternative
[Capteur de tension alternative](https://shop.openenergymonitor.com/ac-ac-power-supply-adapter-ac-voltage-sensor-euro-plug/)

/!\ Il est important d'utiliser un transformateur de tension pour la sécurité humaine et matérielle (afin que la tension du réseau n'aille pas sur la carte).

++Photo transfo + capteur branché

##### Courant continu
http://www.europowercomponents.com/media/uploads/HASS50.pdf

Capteur de type transducteur, permet d'avoir une tension qui est image du courant qui circule dans le fil.


##### Courant alternatif
[Capteur de courant alternatif](https://shop.openenergymonitor.com/100a-max-clip-on-current-sensor-ct/)

#### Où les placer ?
##### Tension continue

##### Tension alternative
prise ronde
Il sert à mesurer la tension du réseau après l'onduleur, pour connaître votre puissance en sortie / pour l'utilisateur.

##### Courant continu
Sur les batteries

##### Courant alternatif
à la sortie de l'onduleur sur la phase.

#### Comment les brancher ?
##### Tension continue

##### Tension alternative

##### Courant continu
Vref sur la broche 1
Output sur la broche 2
0V sur 3
+5V sur 4
(1 sur 1, 2 sur 2 etc)

##### Courant alternatif
Sens de positionnement du capteur, fléches dirigées vers le consommateur et pas vers la source.
++ photo (vérifier si la mesure est positive ou négative)

Brancher sur les broches 1(rouge) et 2(blanc) (indépendamment du sens) couleur valable pour le capteur SCT013


#### Comment les calibrer ?
++lien

### Capteurs de fréquence - en cours de développement
Possibilité de placer le capteur juste après l'éolienne pour connaître. Entrée anémomètre + transfo pour abaisser la tension.

### Capteurs de température - en cours de développement
Il est possible d'installer jusqu'à xxx sondes de température.

#### Quelles sondes de température choisir ?
[Capteur de température](https://shop.openenergymonitor.com/encapsulated-ds18b20-temperature-sensor/)

## Paramétrage du micro contrôleur
A cette étape, vos cartes électroniques sont câblées et calibrées. (Le firmware windlogger est chargé.non??)

### Installation du terminal série
Nous allons tout d'abord installer l'interface qui permettra de communiquer avec le micro contrôleur. Pour cela, téléchargez le terminal série de votre choix :
* Linux : GTkTerm (tapez dans votre terminal la commande suivante)
```console
sudo apt-get install gtkterm
```
* Mac : putty
* Windows : putty + driver silabs cp2102


Si arduino est installé sur votre ordinateur, un terminal série est intégré.

#### Sur GTkTerm
Branchez le datalogger sur un port USB.

Allumez le terminal série.

Dans Configuration > Port, configurer le port USB utilisé :

![alt text](images/configuration_port.png)

Sous linux, pour qu'un utilisateur puisse accéder au port usb (par le terminal série par exemple), il faut l'ajouter au groupe `dialout`.
Pour cela vous pouvez utiliser la commande suivante dans votre terminal bash :

```console
$ sudo usermod -aG dialout nom_utilisateur
```

### Découverte du menu
Pour visualiser le menu, tapez `$$+entrée`
```console
$$
Configuration menu :
	$1 - General
	$2 - Date/Time
	$3 - Anemo1
	$4 - Anemo2
	$5 - Windvane
	$6 - Power1
	$7 - Power2
	$9 - Output configuration
Main menu : $$ or Quit and start measurement : $q
```
Pour visualiser le premier sous-menu, tapez `$1+entrée`
```console
$1
FSM config stuff :
	*11 measure_sample_conf = 4		0: no measure, 1: 10s average,  2:1min average, 3:10min average, 4:no average (each second)
	*12 node id = 15		permit identify each datalogger (0 - 255).
	*19 Reset configuration - reinitialize the FSM and each sensors configuration.Main menu : $$ or Quit and start measurement : $q
```
Chaque ligne (\*11, \*12, \*19) permet de modifier un paramètre, en sélectionnant l'option voulue.

Exemple : Si vous souhaitez modifier la fréquence des mesures, entrez dans le menu général. Vous pouvez choisir la première option "une moyenne de mesure toutes les 10sec" en tapant `*11=1`

```console
$1
FSM config stuff :
	*11 measure_sample_conf = 4		0: no measure, 1: 10s average,  2:1min average, 3:10min average, 4:no average (each second)
	*12 node id = 15		permit identify each datalogger (0 - 255).
	*19 Reset configuration - reinitialize the FSM and each sensors configuration.Main menu : $$ or Quit and start measurement : $q
*11=1
FSM config stuff :
	*11 measure_sample_conf = 1		0: no measure, 1: 10s average,  2:1min average, 3:10min average, 4:no average (each second)
	*12 node id = 15		permit identify each datalogger (0 - 255).
	*19 Reset configuration - reinitialize the FSM and each sensors configuration.Main menu : $$ or Quit and start measurement : $q
```
Vous pouvez naviguer ainsi dans les différents sous-menu :

`$1+entrée` Menu général (General)
```console
*11 Configurer de l'échantillonnage (mesure sample conf)
*12 Donner un identifiant au datalogger compris entre 0 et 255 (node id)
*19 Remise à zéro de tous les paramètres généraux, sauf la date et l'heure (reset configuration)
```
`$2+entrée` Date et heure (Date/Time)
```console
*21 Heure (Time) sous la forme xx:xx:xx
*22 Date, sous la forme MM/JJ/AAAA
```
`$3+entrée` Paramètres de l'anémomètre 1 (Anemo1)
```console
*31 Activer ou désactiver le capteur (Enable)
*32 Appliquer un facteur à la mesure (pour convertir d'une unité à une autre par exemple) (Factor)
*33 Appliquer un offset (=valeur à l'origine) à la mesure (Offset)
```
`$4+entrée` Paramètres de l'anémomètre 2 (Anemo2)
```console
*41 Activer ou désactiver le capteur (Enable)
*42 Appliquer un facteur à la mesure (pour convertir d'une unité à une autre par exemple) (Factor)
*43 Appliquer un offset (=valeur à l'origine) à la mesure (Offset)
```
`$5+entrée` Paramètres de la girouette (Windvane)
```console
*51 Activer ou désactiver le capteur (Enable)
*52 Appliquer un facteur à la mesure (pour convertir d'une unité à une autre par exemple) (Factor)
*53 Appliquer un offset (=valeur à l'origine) à la mesure (Offset)
```
`$6+entrée` Paramètres des mesures de puissance 1 (Power1)
```console
*61 Activer ou désactiver le capteur (Enable)
*62 Choisir une mesure alternative ou continue (AC Power)
*63 Appliquer un facteur à la sonde de tension (voltage factor)
*64 Appliquer un offset (=valeur à l'origine) à la mesure de tension (voltage offset)
*65 Appliquer un facteur pour régler le déphasage entre la tension et l'intensité (voltage phase)
*66 Appliquer un facteur à la sonde de courant (current factor)
*67 Appliquer un offset (=valeur à l'origine) à la mesure de courant (curent offset)
*69 Choisir la précision des mesures (Data accuracy)
```
`$7+entrée` Paramètres des mesures de puissance 2 (Power2)
```console
*71 Activer ou désactiver le capteur (Enable)
*72 > choisir une mesure alternative ou continue (AC Power)
*73 Appliquer un facteur à la sonde de tension (voltage factor)
*74 Appliquer un offset (=valeur à l'origine) à la mesure de tension (voltage offset)
*75 Appliquer un facteur pour régler le déphasage entre la tension et l'intensité (voltage phase)
*76 Appliquer un facteur à la sonde de courant (current factor)
*77 Appliquer un offset (=valeur à l'origine) à la mesure de courant (curent offset)
*79 Choisir la précision des mesures (Data accuracy)
```
`$9+entrée` Configuration des sorties (Output configuration)
```console
*91 Activer ou désactiver l'envoi des données sur le port USB (Serial enable)
*92 Activer ou désactiver l'envoi des données sur la carte SD (Sd card enable)
```
### Le calibrage des capteurs

Pour calibrer chaque capteur, suivre les instructions sur [la page wiki de la carte shield](https://gitlab.com/gilou_/windlogger_shield/wikis/Home_FR).


## La transmission et l'enregistrement des données

### Carte USB

### Carte SD

### Wifi - en cours de développement


### Carte SIM (GPRS) - en cours de développement


## Visualisation des données - en cours de développement
Nécessite compétences en informatiques !

## La maintenance

## Les outils de travail collaboratif
On a besoin de vous ! Compétences..

### Gitlab
Fork
Travail et commit réguliers + push
Merge request

Projects > Explore projects

Présentation du projet et installation du datalogger : (Aurélie)
https://gitlab.com/gilou_/windlogger

Partie numérique : (Gilou)
https://gitlab.com/gilou_/windlogger_digital

Partie analogique avec tous les capteurs : (Gilou)
https://gitlab.com/gilou_/windlogger_shield

Code du micro contrôleur : (Luiz)
https://gitlab.com/gilou_/windlogger_firmware

#### Librairies arduino créées pour le projet

Capteur de puissance ?
https://gitlab.com/gilou_/Power

Anémomètre
https://gitlab.com/gilou_/Anemometer

Girouette
https://gitlab.com/gilou_/Windvane

### Atom
Dans votre terminal, installez le programme gdebi :
```console
sudo apt-get install gdebi
```
Téléchargez le logiciel [Atom](https://atom.io/) (fichier .deb)

Dans votre terminal, tapez :
```console
sudo gdebi atom-amd64.deb
```
