# Présentation du projet et installation du datalogger
## Le projet de windlogger

Le windlogger est un projet open-source d'enregistreur de données pour le petit éolien. Cette idée à émergée lors de discution avec les membres du réseau Tripalium courant 2014. Les outils éxistants à l'époque , étaient soit chers, soit peu adaptés aux mesures que nous souhaitions faire. La présentation d'un premier prototype lors des conférences WEAthens2014 organisés par l'association WindEmpowerment à confirmé la nécessité de cette initiative.

Le principe est de développer un outil simple et abordable pour effectuer des mesures de vent et de puissance d'une éolienne. Ces données peuvent être stockées sur carte SD ou bien transmises (USB, Wifi, GPRS).

Actuellement les données courantes sur le vent sont inadaptées au petit éolien puisque la plupart des données sont données pour des hauteur de 50 ou 80 m. De plus, le coût prohibitifs d'une campagne de mesure nous empèche d'y procéder.

Une autre motivation est que les pilotes d'éolienne souhaitent avoir un outil de visualisation de leur site en termes de ressources vent, et de production. Puisque nous construisons nos éoliennes, cela permet aussi lors d'une mise en service d'une nouvelle éolienne, de vérifier son bon fonctionnement, est-ce que l'éolienne produit trop ou assez? Est-ce que la régulation fonctionne correctement?

Nous sommes donc partis sur une solution libre et modulable pour répondre aux différentes configurations possible.

#### Contributeurs et contributrices

 * LONGUET Gilles, development/maintener(@gilou_)
 * Luiz Villa, development(@luiz.villa)
 * DAME David, contributor
 * HEMBERT Brice, contributor
 * BENHAMED Sofiane, contributor
 * GILBERT Aurélie, Documentation(@aurelieguibert)

## Les acteurs

<table>
 <tr>
  <td min-width="200px"> <img src="images/logo_aleea.png" alt="logo aleea" min-width="200px"> </td>
  <td>Basée dans la région toulousaine, l’association ALEEA a pour objet de rechercher, développer, fabriquer et promouvoir l'électronique pour la gestion de l'énergie auto-produite.

  Elle défend la liberté d'utiliser, étudier, copier, modifier et fabriquer les systèmes électroniques qu'elle a conçus dans un esprit de « Hardware libre » tel qu'il est présenté sur le site gnu.org.

  Elle développe des actions d'échanges de savoirs et de savoir-faire s'inscrivant dans une démarche de développement de l'autonomie des personnes vis-à-vis des systèmes électroniques et de production d'énergie.</td>
 </tr>
 <tr>
  <td min-width="200px"><img src="images/logo_tripalium.png" alt="logo association tripalium" min-width="200px"></td>
  <td>Impulsée en 2007 par la volonté de développer les stages d’auto-construction, l’association Tripalium s’est transformée en un réseau d’acteur-rices. Sans salarié ni subvention, sa vitalité repose sur la dynamique des stages : les anciens stagiaires sont invités à devenir formateurs, puis à organiser des stages...

  En 2014, l’association, compte à son actif l’organisation d’une centaine de stages. Une douzaine de formateurs et formatrices se répartit principalement en Drôme, Haute-Garonne, Lozère, Gard, Loire-Atlantique, Tarn, Finistère, Ardèche. Et plus de 150 éoliennes Piggott ont déjà été construites en France. Les principales activités du réseau sont : les stages, les rencontres créatives (annuelles), l’aide à l’organisation de stages et à l’investissement, le site web, la traduction, l’actualisation et la publication du manuel, des interventions dans divers événements et sous diverses formes.</td>
 </tr>
 <tr>
  <td min-width="200px"><img src="images/logo_WindEmpowerment.png" alt="logo association WindEmpowerment" min-width="200px"></td>
  <td>Windempowerment est une association pour le développement de la fabrication locale d'éolienne de type Piggott pour l'électrification rurale. Nous représentons des dizaines d'organisations membres, constituées de fabricants, d'ONG, d'universités, entreprises sociales, de centre de formations  ainsi que plus de 1000 participants individuels à travers le monde.</td>
 </tr>
</table>

## Combien ça coûte ?
Actuellement l'association Aleea vend les premiers kits pour 200€, cela comprend les cartes **Digital** et **Shield**, des catpeurs pour un wattmètre (DC ou AC) et le boîtier.
contact : contact (at) aleea (dot) org

## Comment ça fonctionne ?
### Rappels sur l'installation électrique d'une éolienne

!["Schéma d'u site isolé par Eve Lomenech"](images/Sch_site_isole_auteure_eve_lomenech.png "Schéma d'un site isolé par Eve Lomenech")*Schéma d'un site isolé par Eve Lomenech*


#### La génératrice
Génére un courant alternatif triphasé à fréquence variable, selon la puissance du vent.
Ce courant à fréquence variable n'est pas utilisable tel quel, il nécessite un passage par différents boitiers électroniques.

#### Le frein
Il permet de freiner l'éolienne en mettant les 3 phases de la génératrice en court-circuit. On l'utilise pour le levage et la descente de l'éolienne.

#### Le pont de diodes, ou pont redresseur
Il permet de transformer un courant alternatif en courant continu.
C'est après ce pont de diodes qu'on branchera les batteries pour stocker l'énergie dans le cas d'un site isolé.
(Le pont de diodes est fixé sur un «  radiateur  » en aluminium pouvant dissiper la chaleur produite.)

#### Le régulateur
Dans le cas d'un site isolé, il protège les batteries. Quand elles sont pleines, l'énergie doit obligatoirement s'échapper quelque part (il ne faut jamais que l’éolienne tourne à vide). Le régulateur commute donc cette énergie dans la résistance de décharge.

Dans le cas d'un site raccordé au réseau, il protège l’onduleur des surtensions par délestage dans une résistance.
Il protège aussi l’éolienne en cas de coupure du réseau électrique, en connectant l’éolienne sur la résistance pour éviter l’emballement.

#### L'onduleur
L'onduleur transforme le courant continu en courant alternatif 230V / 50Hz, que l'on pourra directement utiliser ou injecter sur le réseau électrique dans le cas d'un raccordement réseau.

Pour plus de détails, consulter le [manuel Tripalium](https://www.tripalium.org/manuel).

### L’installation du boîtier et des capteurs

![Schéma d'un site isolé par Eve Lomenech, colorisé par Aurélie Guibert](images/Sch_site_isole_capteurs_auteure_eve_lomenech.png "Schéma d'un site isolé par Eve Lomenech, colorisé par Aurélie Guibert")*Schéma d'un site isolé par Eve Lomenech, colorisé par Aurélie Guibert*

## Les cartes électroniques (Hardware)

### La partie numérique digital board :
La carte *digital* reçoit organise les mesures et gère l'enregistrement des données ainsi que leurs transmition.
De base, cette carte peut enregistrer les données sur carte SD et les envoyer par USB.

**Les modules Wifi et GPRS sont en option.**

!["Carte digital"](images/board_digital.png "Carte digital")*Carte digital*


### La partie analogique shield board :
Elle vient s’imbriquer sur la carte *digital*. C'est elle qui reçoit les différents capteurs, elle met en forme leurs signaux pour pouvoir les lire avec le micro-contrôleur de la carte *digital*.

!["Carte shield"](images/board_shield.png "Carte shield")*Carte shield*

## Le code du micro-contrôleur (« Firmware »)

Le firmware est développer pour être compatible avec l'environnement Arduino. Cela le rend donc accessible et évolutif.

## Le traitement des données (« Software ») - en cours de développement

La partie "Software" permet d'analyser et de visualiser les données sur un ordinateur. (rose des vents dominants (puissance/fréquence), graphiques de production sur l'année, production de l'éolienne en fonction des vitesses de vent, détection de disfonctionnements et corrélation avec différents facteurs, etc.)

**Pour l'instant nous ne nous sommes pas occupé de cette partie.**

**Si cette partie vous intéresse, n'hésitez pas à nous contacter.**
